# Componente - FUNCEF-PageHeader

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Page Header possibilita a inserção de botões na parte superior da página e também de informação de texto.
 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-page-header --save.
```

## Script

```html
<script src="bower_components/funcef-page-header/dist/funcef-page-header.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-page-header/dist/funcef-page-header.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funfef-header dentro do módulo do Sistema.

```js
angular
        .module('funcef-demo', ['funcef-page-header']);
```

## Uso

```html
<ngf-page-header title="Nome do Sistema" voltar="home">
	<button ui-sref="" class="btn btn-primary">
        Botão 1
    </button>
    <button ui-sref="" class="btn btn-primary">
        Botão 2
    </button>
    <button ui-sref="" class="btn btn-primary">
        Botão 3
    </button>
</ngf-page-header>

```

### Parâmetros adicionais:

- title (Obrigatório);
- voltar (Opcional);

## Desinstalação:

```
bower uninstall funcef-page-header --save
```
