﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @version 1.0.0
    */
    angular.module('funcef-page-header.controller', []);
    angular.module('funcef-page-header.directive', []);

    angular
    .module('funcef-page-header', [
      'funcef-page-header.controller',
      'funcef-page-header.directive'
    ]);
})();