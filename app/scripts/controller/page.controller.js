(function () {
    'use strict';

    angular.module('funcef-page-header.controller')
        .controller('PageHeaderController', PageHeaderController);

    PageHeaderController.$inject = ['$rootScope', '$attrs'];

    /* @ngInject */
    function PageHeaderController($rootScope, $attrs) {
        var vm = this;
        $rootScope.title = $attrs.title;

        $attrs.$observe('title', function (value) {
            $rootScope.title = $attrs.title;
        });
    };

}());