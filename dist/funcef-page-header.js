(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @version 1.0.0
    */
    angular.module('funcef-page-header.controller', []);
    angular.module('funcef-page-header.directive', []);

    angular
    .module('funcef-page-header', [
      'funcef-page-header.controller',
      'funcef-page-header.directive'
    ]);
})();;(function () {
    'use strict';

    angular.module('funcef-page-header.controller')
        .controller('PageHeaderController', PageHeaderController);

    PageHeaderController.$inject = ['$rootScope', '$attrs'];

    /* @ngInject */
    function PageHeaderController($rootScope, $attrs) {
        var vm = this;
        $rootScope.title = $attrs.title;

        $attrs.$observe('title', function (value) {
            $rootScope.title = $attrs.title;
        });
    };

}());;(function () {
    'use strict';

    angular
      .module('funcef-page-header.directive')
      .directive('ngfPageHeader', ngfPageHeader);

    /* @ngInject */
    function ngfPageHeader() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/page.view.html',
            controller: 'PageHeaderController',
            controllerAs: 'vm',
            scope: {
                title: '@',
                voltar: '@',
            },
            link: function (scope, element, attrs, ctrl, transclude) {
                element.find('.content').after(transclude()).remove();
            }
        };
    }
})();;angular.module('funcef-page-header').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/page.view.html',
    "<header class=\"header-content\"> <div class=\"pull-left\"> <h2 class=\"page-header\"> {{title}} </h2> <div ncy-breadcrumb></div> </div> <div class=\"pull-right btn-group\"> <a ui-sref=\"{{voltar}}\" ng-if=\"voltar\" class=\"btn btn-default\"> <i class=\"fa fa-reply\"></i> Voltar </a> <div class=\"content pull-right\"></div> </div> </header>"
  );

}]);
